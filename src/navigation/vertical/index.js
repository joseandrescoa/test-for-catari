const navigation = () => {
  return [
    {
      sectionTitle: 'Apps & Pages'
    },
    {
      title: 'User',
      icon: 'mdi:account-outline',
      children: [
        {
          title: 'List',
          path: '/apps/user/list'
        }
      ]
    }
  ]
}

export default navigation
