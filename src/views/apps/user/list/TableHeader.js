// ** MUI Imports
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'

import TextField from '@mui/material/TextField'

// ** Icon Imports
import Icon from 'src/@core/components/icon'

const TableHeader = props => {
  // ** Props
  const { handleFilter, toggle, value } = props

  return (
    <Box sx={{ p: 5, pb: 3, display: 'flex', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'space-between' }}>
      <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
        <Button
          sx={{ mr: 4, mb: 2 }}
          color='secondary'
          variant='outlined'
          startIcon={<Icon icon='mdi:export-variant' fontSize={20} />}
        >
          Export
        </Button>
        <Button
          sx={{ mr: 4, mb: 2 }}
          color='secondary'
          variant='outlined'
          startIcon={<Icon icon='mdi:cloud-upload' fontSize={20} />}
        >
          Upload
        </Button>
      </Box>
      <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
        <Button sx={{ mb: 2, marginRight:5, width: '126px' }} variant='outlined'>
          Remove
        </Button>
        <Button sx={{ mb: 2, marginRight:5, width: '126px' }} onClick={toggle} variant='outlined'>
          Add User
        </Button>
        <Button sx={{ mb: 2, width: '126px' }} variant='contained'>
          Search
        </Button>
      </Box>
    </Box>
  )
}

export default TableHeader
